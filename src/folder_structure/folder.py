from __future__ import annotations

import os
import logging
from collections.abc import Hashable
from pathlib import Path
from pydantic import BaseModel, Field, ConfigDict
from typing import Optional
from uuid import uuid4, UUID

logger = logging.getLogger(__name__)


class Folder(BaseModel, Hashable):
    model_config = ConfigDict(extra='allow')

    name: str
    parent: Optional[Folder]=None
    id: Optional[UUID]=Field(default_factory=uuid4)

    def __hash__(self):
        return int(self.id)
    
    def path(self, *, root: str="."):
        # Build the path, starting at the leaf since the child tracks its
        # parent.
        path = [self.name]
        parent = self.parent
        while parent:
            path.append(parent.name)
            parent = parent.parent
        # Create the folder in reversed order.
        # path = os.sep.join(reversed(path))
        return Path(root).joinpath(*reversed(path))
    
    def build(self, mode=0o777, parents=False, exist_ok=False, *, root: str="."):
        logger.debug(f"Making directory: "
                     f"(mode, parents, exists_ok) = "
                     f"({mode}, {parents}, {exist_ok})")
        path = self.path(root=root)
        logger.info(f"Making directory {str(path)!r}.")
        path.mkdir(mode=mode, parents=parents, exist_ok=exist_ok)
