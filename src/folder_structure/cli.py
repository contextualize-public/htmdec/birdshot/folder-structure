"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
``[options.entry_points]`` section in ``setup.cfg``::

    console_scripts =
         fibonacci = folder_structure.skeleton:run

Then run ``pip install .`` (or ``pip install -e .`` for editable mode)
which will install the command ``fibonacci`` inside your current environment.

Besides console scripts, the header (i.e. until ``_logger``...) of this file can
also be used as template for Python modules.

Note:
    This file can be renamed depending on your needs or safely removed if not needed.

References:
    - https://setuptools.pypa.io/en/latest/userguide/entry_point.html
    - https://pip.pypa.io/en/stable/reference/pip_install
"""

import argparse
import json
import logging
import sys

from folder_structure import __version__
from folder_structure.schema import Schema
from pathlib import Path

__author__ = "Branden Kappes"
__copyright__ = "Branden Kappes"
__license__ = "MIT"

_logger = logging.getLogger(__name__)


# ---- Python API ----
# The functions defined in this section can be imported by users in their
# Python scripts/interactive interpreter, e.g. via
# `from folder_structure.skeleton import fib`,
# when using this Python module as a library.


def build(schema: Schema, obj: str, name: str, *, root: str, exist_ok: bool=False):
    schema[obj](name=name).build(root=root, exist_ok=exist_ok)


# ---- CLI ----
# The functions defined in this section are wrappers around the main Python
# API allowing them to be called directly from the terminal as a CLI
# executable/script.


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="Builds an object based on its defintion in a schema.")
    # Positional
    parser.add_argument(
        "type",
        help="Type of the item that is to be constructed."
    )
    parser.add_argument(
        "name",
        help="Name of the item that is to be constructed."
    )
    # Options
    parser.add_argument(
        "--exist-ok", dest="exist_ok",
        action="store_true",
        default=False,
        help="If set (default, False), do not create the enclosing parent folder."
    )
    parser.add_argument(
        "-e", "--env",
        action="append",
        help="Add an environment variable to be expanded in the schema. "
             "Multiple variables can be specified, formatted as 'KEY=value' pairs. "
             "These should be specified in the schema.json as $KEY."
    )
    parser.add_argument(
        "--schema",
        help="The JSON-formatted file that contains the schema definition."
    )
    parser.add_argument(
        "--where",
        dest="root",
        default=".",
        help="The parent folder into which the new object should be built. " \
             "Default: current directory."
    )
    # Common options
    parser.add_argument(
        "--version",
        action="version",
        version=f"folder-structure {__version__}",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        dest="loglevel",
        help="set loglevel to INFO",
        action="store_const",
        const=logging.INFO,
    )
    parser.add_argument(
        "-vv",
        "--very-verbose",
        dest="loglevel",
        help="set loglevel to DEBUG",
        action="store_const",
        const=logging.DEBUG,
    )
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """Wrapper allowing :func:`fib` to be called with string arguments in a CLI fashion

    Instead of returning the value from :func:`fib`, it prints the result to the
    ``stdout`` in a nicely formatted message.

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--verbose", "42"]``).
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.info(f"Building {args.name!r} (type: {args.type!r}) in {args.root!r}.")
    # Allow for variable substitution in the schema.
    with open(args.schema) as ifs:
        contents = ifs.read()
    for var in args.env:
        key, value = var.split("=")
        _logger.info(f"Replacing {var}: {key} -> {value}")
        contents = contents.replace(f"${key}", value)
    pkg = json.loads(contents)
    # Build the schema
    schema = Schema.generate(pkg)
    # Build the folder structure.
    build(schema, args.type, args.name, root=args.root, exist_ok=args.exist_ok)
    _logger.info("Finished.")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m folder_structure.skeleton 42
    #
    run()
