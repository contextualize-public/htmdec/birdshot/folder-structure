from __future__ import annotations

import json
from .folder import Folder
from pprint import pformat

class Schema:
    """
    This class is used to process an appropriately formatted schema document
    (JSON) that defines the contents (relationships) that exist in a project.


    """
    knownClasses = dict()
    
    def __getitem__(self, key):
        return self.knownClasses[key]
    
    @staticmethod
    def is_folder(key_class_instance):
        obj = Schema.knownClasses[key_class_instance] if isinstance(key_class_instance, str) else key_class_instance
        return (isinstance(obj, Folder) or issubclass(obj, Folder))
    
    @staticmethod
    def generate(schema, reset: bool=True):
        """
        Generate the elements defined in a JSON-formatted schema where each
        entry has the following structure:
        
            {
                "type": (class name, str),
                "base": (name of the base class, str, or Folder),
                "contains": (contents of the folder, dict[str, str])
            }

        The "contains" field defines the entries by name. This, then, requires
        all subfolder names to be unique within a folder.

            "contains": {
                name1: type1,
                name2: type2,
                ...
            }
        
        Parameters
        ----------
        schema : str or list
            Filename containing the JSON schema or a JSON-formatted object.
        
        reset : bool
            Whether to reset the existing schema. Default is True, that is, each
            call will generate a new Schema. If False, then the classes created
            will build upon one another from one file to the next.
            
        Returns
        -------
        instance : Schema
            Instance of the Schema type.
        """
        if reset or len(Schema.knownClasses) == 0:
            Schema.knownClasses = {
                "Folder": Folder
            }
        # Open the JSON file that contains the schema
        if isinstance(schema, str):
            with open(schema) as ifs:
                queue = json.load(ifs)
        else:
            queue = schema
        if not isinstance(queue, list):
            raise ValueError("The schema must be a list of JSON-formatted types.")
        # Counter to check for a missing definition
        # This allows folder types to be specified in any order.
        maxlength = len(queue)
        missing = set()
        while True:
            # get the first entry
            try:
                entry = queue.pop(0)
            except IndexError:
                # all entries have been processed
                break
            # get the key/value pairs from the schema
            try:
                classname = entry["type"]
                basename = entry["base"]
                contents = entry.get("contains", dict())
                if not isinstance(contents, dict):
                    raise KeyError()
            except KeyError:
                raise IOError("Entry {} does not have the proper "
                              "format.".format(pformat(entry)))
            if classname in Schema.knownClasses:
                raise KeyError(f"{classname!r} is defined multiple times.")
            # create the class
            try:
                base = Schema()[basename]

                if Schema.is_folder(base):
                    methods = {
                        '__new__': Schema._folder_new_factory(base),
                        '__init__': Schema._folder_init_factory(contents, base),
                        'build': Schema._folder_build_factory()
                    }
                else:
                    raise ValueError("{} is not a recognized type.".format(base))
                
                Schema.knownClasses[classname] = type(
                    classname,
                    (base,),
                    methods
                )
                
                # successfully generated a class
                # reset the counter that checks for missing definitions
                maxlength = len(queue)
                missing = missing - {classname}
            except (KeyError, TypeError):
                # A key error means we haven't reached the definition
                # of an entry yet. maxlength is the maximum length of the
                # queue before realizing that a class has not been
                # defined.
                queue.append(entry)
                maxlength = maxlength - 1
                missing.add(classname)
                if maxlength < 1:
                    # we've gone through every entry in the schema at least once
                    # without defining a type that was used.
                    raise IOError("The following objects have not been defined "
                                  "in the schema: {}.".format(list(missing)))
        return Schema()
    
    @staticmethod
    def _folder_new_factory(base):
        def new(cls, *args, **kwds):
            # Check for self-reference
            if cls in cls.mro()[1:]:
                # Class MRO, including the current, are listed with this class first.
                # Because classes are created and resolved dynamically, it is possible
                # for a class to refer to itself.
                raise RuntimeError(f"Definition of {cls!r} contains a self-reference.")
            return base.__new__(cls)
        return new
    
    @staticmethod
    def _folder_init_factory(contents, base):
        def init(self, *, name: str, parent: Folder=None):
            # Check for self-reference
            if type(self) in type(self).mro()[1:]:
                # Class MRO, including the current, are listed with this class first.
                # Because classes are created and resolved dynamically, it is possible
                # for a class to refer to itself.
                raise RuntimeError(f"Definition of {type(self)!r} contains a self-reference.")
            base.__init__(self, name=name, parent=parent)
            self.contents = getattr(self, "contents", list()) # contents from base class
            for name, typename in contents.items():
                cls = Schema()[typename]
                self.contents.append(cls(name=name, parent=self))
                # self.contents.append(cls(name=name, parent=parent))
        return init
    
    @staticmethod
    def _folder_build_factory():
        def build(self, mode=0o777, parents=False, exist_ok=False, *, root: str="."):
            Folder.build(self, mode=mode, parents=parents, exist_ok=exist_ok, root=root)
            for child in self.contents:
                child.build(mode=mode, parents=parents, exist_ok=True, root=root)
        return build
    