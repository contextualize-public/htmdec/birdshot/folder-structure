.. These are examples of badges you might want to add to your README:
   please update the URLs accordingly

    .. image:: https://api.cirrus-ci.com/github/<USER>/folder-structure.svg?branch=main
        :alt: Built Status
        :target: https://cirrus-ci.com/github/<USER>/folder-structure
    .. image:: https://readthedocs.org/projects/folder-structure/badge/?version=latest
        :alt: ReadTheDocs
        :target: https://folder-structure.readthedocs.io/en/stable/
    .. image:: https://img.shields.io/coveralls/github/<USER>/folder-structure/main.svg
        :alt: Coveralls
        :target: https://coveralls.io/r/<USER>/folder-structure
    .. image:: https://img.shields.io/pypi/v/folder-structure.svg
        :alt: PyPI-Server
        :target: https://pypi.org/project/folder-structure/
    .. image:: https://img.shields.io/conda/vn/conda-forge/folder-structure.svg
        :alt: Conda-Forge
        :target: https://anaconda.org/conda-forge/folder-structure
    .. image:: https://pepy.tech/badge/folder-structure/month
        :alt: Monthly Downloads
        :target: https://pepy.tech/project/folder-structure
    .. image:: https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter
        :alt: Twitter
        :target: https://twitter.com/folder-structure

.. image:: https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold
    :alt: Project generated with PyScaffold
    :target: https://pyscaffold.org/

|

================
folder-structure
================


    Add a short description here!


A longer description of your project goes here...


.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.5. For details and usage
information on PyScaffold see https://pyscaffold.org/.
