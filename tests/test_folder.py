import pytest
from pathlib import Path

from folder_structure.folder import Folder

__author__ = "Branden Kappes"
__copyright__ = "Branden Kappes"
__license__ = "MIT"


HASHES = list()


def test_single():
    # Single, current directory
    foo = Folder(name="foo")
    HASHES.append(hash(foo))
    foo.make()
    assert Path("foo").is_dir()
    # No, this is not efficient. But I don't want to walk
    # folders created to avoid recursing into the rest of
    # my file system.
    Path("foo").rmdir()


def test_depth():
    # With depth
    foo = Folder(name="foo")
    bar = Folder(name="bar", parent=foo)
    HASHES.append(hash(foo))
    HASHES.append(hash(bar))
    bar.make(parents=True)
    assert Path("foo/bar").is_dir()
    Path("foo/bar").rmdir()
    Path("foo").rmdir()


def test_tree():
    # build tree
    foo = Folder(name="foo")
    bar = Folder(name="bar", parent=foo)
    baz = Folder(name="baz", parent=foo)
    HASHES.append(hash(foo))
    HASHES.append(hash(bar))
    HASHES.append(hash(baz))
    bar.make(parents=True)
    baz.make()
    assert Path("foo/bar").is_dir()
    assert Path("foo/baz").is_dir()
    Path("foo/bar").rmdir()
    Path("foo/baz").rmdir()
    Path("foo").rmdir()


def test_hashes():
    assert len(HASHES) == len(set(HASHES))


# def test_main(capsys):
#     """CLI Tests"""
#     # capsys is a pytest fixture that allows asserts against stdout/stderr
#     # https://docs.pytest.org/en/stable/capture.html
#     main(["7"])
#     captured = capsys.readouterr()
#     assert "The 7-th Fibonacci number is 13" in captured.out
