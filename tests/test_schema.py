import pytest
from folder_structure.schema import Schema
from pathlib import Path
from shutil import rmtree


@pytest.fixture
def simple_schema():
    schema = [
        {
            "type": "Foo",
            "base": "Folder",
            "contains": {
                "bar": "Bar",
                "baz": "Baz"
            }
        },
        {
            "type": "Bar",
            "base": "Folder"
        },
        {
            "type": "Baz",
            "base": "Folder"
        }
    ]
    # These are the expected paths--excluding the parent, which the
    # user defines when they run.
    paths = [
        "bar",
        "baz"
    ]
    return (schema, paths)


@pytest.fixture
def inheritance_schema():
    schema = [
        {
            "type": "Foo",
            "base": "Folder",
            "contains": {
                "bar": "Bar",
                "bat": "Bat"
            }
        },
        {
            "type": "Bat",
            "base": "Folder",
            "contains": {
                "spotted": "BatStats",
                "brown": "BatStats"
            }
        },
        {
            "type": "Bar",
            "base": "Folder",
            "contains": {
                "pictures": "Folder"
            }
        },
        {
            "type": "BatStats",
            "base": "Bar",
            "contains": {
                "region": "Folder",
                "vitals": "Folder"
            }
        }
    ]
    # These are the expected paths--excluding the parent, which the
    # user defines when they run.
    paths = [
        "bar",
        "bat",
        "bar/pictures",
        "bat/spotted",
        "bat/brown",
        "bat/spotted/pictures",
        "bat/spotted/region",
        "bat/spotted/vitals",
        "bat/brown/pictures",
        "bat/brown/region",
        "bat/brown/vitals"
    ]
    return (schema, paths)
    


def test_simple_schema(simple_schema):
    schema_, paths = simple_schema
    schema = Schema.generate(schema_)
    foo = schema["Foo"](name="foo")
    try:
        foo.build()
        # Check that the expected folders were constructed.
        for folder in paths:
            assert Path("foo").joinpath(folder).is_dir()
    finally:
        # cleanup
        rmtree("foo")


def test_inheritance_schema(inheritance_schema):
    # expected paths from inheritance schema
    schemaRep, paths = inheritance_schema
    schema = Schema.generate(schemaRep)
    foo = schema["Foo"](name="foo")
    try:
        foo.build()
        # Check that the expected folders were constructed.
        for folder in paths:
            assert Path("foo").joinpath(folder).is_dir(), \
                f"'foo/{folder}' is not a directory."
    finally:
        # cleanup
        rmtree("foo")


def test_other_root(simple_schema):
    schema_, paths = simple_schema
    schema = Schema.generate(schema_)
    root = "root"
    Path(root).mkdir()
    foo = schema["Foo"](name="foo")
    try:
        foo.build(root=root)
        # Check that the expected folders were constructed.
        for folder in paths:
            assert Path(root).joinpath("foo", folder).is_dir()
    finally:
        # cleanup
        rmtree(root)
